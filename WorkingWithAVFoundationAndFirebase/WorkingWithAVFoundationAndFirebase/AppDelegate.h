//
//  AppDelegate.h
//  WorkingWithAVFoundationAndFirebase
//
//  Created by M. Buğra Atay on 06/04/2017.
//  Copyright © 2017 M. Buğra Atay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

